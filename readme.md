# Hibiol
![Header](./readme/Header.png)

>>>
縦糸は流れゆく月日。
季節を変え 空色を染める。
横糸は人の生業。
地を踏みしめて、心を揺らして。

The warp threads are the flow of time.
They are dyed by the sky's color as the seasons change.
The weft threads are one's calling.
Their steps upon the earth, the swaying of their hearts.
>>>

Hibiol is an extension which visualizes your browsing history on the new tab page. A timeline displays information for today and a day within the past week. For both days, the 3 websites you spend the most time on and the 2 websites you visit most frequently are shown. All data is kept on your device.

## Setup

```shell
git clone https://gitlab.com/suno.txt/hibiol.git hibiol
cd hibiol
npm install
```

## Component Tree

```
App
├── Theme
├── Content_Header
└── Content
    ├── Home
    │   ├── Chronicle
    │   │   └── Loom (x5)
    │   └── Clock
    └── Settings
```

## Scripts

Hibiol adds a few additional scripts to the ones included with `create-react-app` (`npm start`, `npm test`, `npm run build`, and `npm run eject`).

### `npm run build_scripts`
Builds only the background and content scripts instead of the entire extension.

### `npm run package`
Packages the contents of `hibiol/build` into an extension, writing it to `hibiol/artifacts`.

### `npm run complete`
Builds and packages the extension.

## Themes
![Themes](./readme/Themes.png)
![Themes 2](./readme/Themes 2.png)