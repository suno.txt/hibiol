// Packages.
import browser from "webextension-polyfill";

/*
	Save position for solar altitude angle calculations, even when offline.
*/

navigator.geolocation.watchPosition(position => {
	browser.storage.local.set({
		latitude: position.coords.latitude,
		longitude: position.coords.longitude
	});
});