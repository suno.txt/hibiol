// Packages.
import browser from "webextension-polyfill";
import Vibrant from "node-vibrant";
// etc.
import Open from "../_database";

// Log messages.
const log = false;

/*
	The desired URL schemes to match, including the ending ":".
*/

const schemes = [
	"http:",
	"https:"
];

Open().then(database => {

	/*
		History is memorized as a collection of events.

		An event is a period of time on a specific website. Websites are identified by their domain.
		i.e. "https://website.jp[:port]/[path]" and "http://website.jp[:port]/[path]" are both on "website.jp".
	*/

	/*
		Hibiol 1

		Events
			begin			※
			end				†*
			website			†

		Colors
			website			※
			color

		Hidden
			website			※
	*/

	/*
		The current event.
	*/

	var event = {
		begin: undefined,
		end: undefined,
		website: undefined
	};

	/*
		Memorize an event.

		Input:
		event
			An event to memorize.

		※ Resolves errors to prevent halting.
	*/

	function Memorize(event) {
		return new Promise((resolve, reject) => {
			// Open a transaction.
			let transaction = database.transaction("Events", "readwrite");
			transaction.addEventListener("abort", event => {
				resolve(event.target.error);
			});
			transaction.addEventListener("complete", event => {
				resolve();
			});

			// Log.
			if (log) {
				console.log("Memorize", event);
			}

			// Memorize the event.
			let Events = transaction.objectStore("Events");
			Events.put(event);
		});
	}

	/*
		Dye events on a website a specific color.

		Inputs:
		url
			The website whose events should be dyed.
		color
			A hexadecimal color string, including the beginning "#".

		※ Resolves errors to prevent halting.
	*/

	function Dye(url, color) {
		return new Promise((resolve, reject) => {
			// Open a transaction.
			let transaction = database.transaction("Colors", "readwrite");
			transaction.addEventListener("abort", event => {
				resolve(event.target.error);
			});
			transaction.addEventListener("complete", event => {
				resolve();
			});

			// Save the website's color.
			let Colors = transaction.objectStore("Colors");
			Colors.put({
				website: url.hostname,
				color: color
			});
		});
	}

	/*
		Forget events that ended 8+ days ago (UTC ±0:00), then forget website colors without events.

		8 days = 7 days + ⌈-(UTC-12:00)/(24 hours)⌉ days (when the latest timezone passes 7 days)

		※ Resolves errors to prevent halting.
	*/

	async function Forget() {
		// Forget events.
		await new Promise((resolve, reject) => {
			// Open a transaction.
			let transaction = database.transaction(["Events"], "readwrite");
			transaction.addEventListener("abort", event => {
				resolve(event.target.error);
			});
			transaction.addEventListener("complete", event => {
				resolve();
			});

			// Find the last begin.
			let past = new Date();
			past = new Date(Date.UTC(past.getUTCFullYear(), past.getUTCMonth(), past.getUTCDate() - 8));
			let Events = transaction.objectStore("Events");
			let end = Events.index("end");
			let request = end.openCursor(IDBKeyRange.upperBound(past), "prev");
			request.addEventListener("success", event => {
				// No cursor is returned if no matching records exist.
				if (event.target.result) {
					// Forget events that ended 8+ days ago (UTC).
					let last_begin = event.target.result.value.begin;
					Events.delete(IDBKeyRange.upperBound(last_begin));
				}
			});
		});

		// Forget colors.
		await new Promise((resolve, reject) => {
			// Open a transaction.
			let transaction = database.transaction(["Events", "Colors"], "readwrite");
			transaction.addEventListener("abort", event => {
				resolve(event.target.error);
			});
			transaction.addEventListener("complete", event => {
				resolve();
			});

			// Find all colors to remember.
			let Events = transaction.objectStore("Events");
			let request = Events.getAll();
			request.addEventListener("success", event => {
				let remember = event.target.result.map(e => e.website);

				// Find all colors to forget.
				let Colors = transaction.objectStore("Colors");
				let request = Colors.getAllKeys();
				request.addEventListener("success", event => {
					if (event.target.result.length) {
						let forget = new Set(event.target.result);
						remember.forEach(website => {
							forget.delete(website);
						});

						// Forget colors.
						forget.forEach(website => {
							Colors.delete(website);
						});
					}
				});
			});
		});
	}

	/*
		Change events.

		Inputs:
		date
			The date+time when the listener activated.
		url (optional)
			The new website. If unspecified, the event is changed to eventless.

		1. If another event was happening:
			a. Clear the reminder to memorize once per minute.
			b. Set its end time.
			c. Memorize the event.
		2. If the new website's URL scheme matches:
			a. Change events.
			b. Memorize the event.
			c. Create a reminder to memorize once per minute.
		3. Otherwise:
			a. Change to eventless.
	*/

	function Change(date, url = {}) {
		// If another event was happening:
		if (event.website) {
			// Clear the reminder to memorize once per minute.
			browser.alarms.clear("memorize");
			// Set its end time.
			event.end = new Date(Math.max(event.end, date));
			// Memorize the event.
			Memorize(event)
				// Log.
				.then(message => {
					if (log) {
						if (message) {
							console.log(message);
						}
					}
				});
		}
		// If the new website's URL scheme matches:
		if (schemes.includes(url.protocol)) {
			// Change events.
			event.begin = date;
			event.end = new Date(date.getTime() + 1);
			event.website = url.hostname;
			// Memorize the event.
			Memorize(event)
				// Log.
				.then(message => {
					if (log) {
						if (message) {
							console.log(message);
						}
					}
				});
			// Create a reminder to memorize once per minute.
			browser.alarms.create("memorize", {periodInMinutes: 1});
		}
		// Otherwise:
		else {
			// Change to eventless.
			event.website = undefined;
		}
	}

	/*
		Alarms:
		memorize
			Memorize the current event periodically.
		forget
			Forget events that ended 8+ days ago (UTC ±0:00) periodically.
	*/

	browser.alarms.onAlarm.addListener(alarmInfo => {
		// Save the current date+time.
		let date = new Date();

		/*
			Memorize.
		*/

		if (alarmInfo.name === "memorize") {
			// Set the event's end time.
			event.end = new Date(Math.max(event.end, date));
			// Memorize the event.
			Memorize(event)
				// Log.
				.then(message => {
					if (log) {
						if (message) {
							console.log(message);
						}
					}
				});
		}

		/*
			Forget.
		*/

		if (alarmInfo.name === "forget") {
			// Forget events that ended 8+ days ago (UTC ±0).
			Forget()
				// Log.
				.then(message => {
					if (log) {
						if (message) {
							console.log(message);
						}
					}
				});
		}
	});

	/*
		When the system's state changes:
		1. Save the current date+time.
		2. If the system is locked:
			a. Change to eventless.
		3. If the system is idle or active:
			a. Find the last focused window and check if it's in focus (i.e. if the browser is in focus). Populate the Window object with its tabs.
			b. Check if the domain (i.e. the website) changed.
			c. Change events.
	*/

	browser.idle.onStateChanged.addListener(async newState => {
		// Save the current date+time.
		let date = new Date();
		// If the system is locked:
		if (newState === "locked") {
			// Change to eventless.
			Change(date);

			// Log.
			if (log) {
				console.log(newState + "\n" + "System changed from idle/active to locked." + "\n" + event.website);
			}
		}
		// If the system is idle or active:
		else {
			// Find the last focused window and check if it's in focus (i.e. if the browser is in focus). Populate the Window object with its tabs.
			await browser.windows.getLastFocused({populate: true})
				.then(windowInfo => {
					if (windowInfo.focused) {
						const active_tab = windowInfo.tabs.filter(tab => tab.active)[0];
						// If the active tab has a URL:
						if (active_tab.url) {
							// Check if the domain (i.e. the website) changed.
							let url = new URL(active_tab.url);
							if (url.hostname !== event.website) {
								// Change events.
								Change(date, url);

								// Log.
								if (log) {
									console.log(newState + "\n" + "System changed from locked to active, idle to active, or active to idle." + "\n" + event.website);
								}
							}
						}
						// If the active tab doesn't have a URL:
						else {
							// Change to eventless.
							Change(date);
						}
					}
				});
		}
	});

	/*
		When the currently focused window changes:
		1. Save the current date+time.
		2. Check if the system is unlocked.
		3. If focus changed to another program:
			a. Change to eventless.
		4. If focus changed to a browser window:
			a. Find the window. Populate the Window object with its tabs.
			b. Check if the domain (i.e. the website) changed.
			c. Change events.
	*/

	browser.windows.onFocusChanged.addListener(async windowId => {
		// Save the current date+time.
		let date = new Date();
		// Check if the system is unlocked.
		await browser.idle.queryState(60)
			.then(async newState => {
				if (newState !== "locked") {
					// If focus changed to another program:
					if (windowId === browser.windows.WINDOW_ID_NONE) {
						// Change to eventless.
						Change(date);

						// Log.
						if (log) {
							console.log(windowId.toString() + "\n" + "Browser defocused or developer tools focused." + "\n" + event.website);
						}
					}
					// If focus changed to a browser window:
					else {
						// Find the window. Populate the Window object with its tabs.
						await browser.windows.get(windowId, {populate: true})
							.then(windowInfo => {
								const active_tab = windowInfo.tabs.filter(tab => tab.active)[0];
								// If the active tab has a URL:
								if (active_tab.url) {
									// Check if the domain (i.e. the website) changed.
									let url = new URL(active_tab.url);
									if (url.hostname !== event.website) {
										// Change events.
										Change(date, url);

										// Log.
										if (log) {
											console.log(windowId.toString() + "\n" + "Browser focused." + "\n" + event.website);
										}
									}
								}
								// If the active tab doesn't have a URL:
								else {
									// Change to eventless.
									Change(date);
								}
							});
					}
				}
			});
	});

	/*
		When a tab in any window is activated:
		1. Save the current date+time.
		2. Check if the system is unlocked.
		3. Find the last focused window and check if it's in focus (i.e. if the browser is in focus). Populate the Window object with its tabs.
		4. Check if the activated tab is in the last focused window.
		5. Check if the domain (i.e. the website) changed.
		6. Change events.
	*/

	browser.tabs.onActivated.addListener(async activeInfo => {
		// Save the current date+time.
		let date = new Date();
		// Check if the system is unlocked.
		await browser.idle.queryState(60)
			.then(async newState => {
				if (newState !== "locked") {
					// Find the last focused window and check if it's in focus (i.e. if the browser is in focus). Populate the Window object with its tabs.
					await browser.windows.getLastFocused({populate: true})
						.then(windowInfo => {
							if (windowInfo.focused) {
								// Check if the activated tab is in the last focused window.
								if (activeInfo.windowId === windowInfo.id) {
									const active_tab = windowInfo.tabs.filter(tab => tab.active)[0];
									// If the active tab has a URL:
									if (active_tab.url) {
										// Check if the domain (i.e. the website) changed.
										let url = new URL(active_tab.url);
										if (url.hostname !== event.website) {
											// Change events.
											Change(date, url);

											// Log.
											if (log) {
												console.log(activeInfo.tabId.toString() + "\n" + "Website change from active tab switch." + "\n" + event.website);
											}
										}
									}
									// If the active tab doesn't have a URL:
									else {
										// Change to eventless.
										Change(date);
									}
								}
							}
						});
				}
			});
	});

	/*
		When a tab in any window is updated:
		1. Save the current date+time.
		2. Check if the update is a URL change (i.e. a potential website change).
		3. Check if the system is unlocked.
		4. Find the last focused window and check if it's in focus (i.e. if the browser is in focus). Populate the Window object with its tabs.
		5. Check if the updated tab is the last focused window's active tab.
		6. Check if the domain (i.e. the website) changed.
		7. Change events.
	*/

	browser.tabs.onUpdated.addListener(async (tabId, changeInfo, tabInfo) => {
		// Save the current date+time.
		let date = new Date();
		// Check if the update is a URL change (i.e. a potential website change).
		if (changeInfo.url) {
			// Check if the system is unlocked.
			await browser.idle.queryState(60)
				.then(async newState => {
					if (newState !== "locked") {
						// Find the last focused window and check if it's in focus (i.e. if the browser is in focus). Populate the Window object with its tabs.
						await browser.windows.getLastFocused({populate: true})
							.then(windowInfo => {
								if (windowInfo.focused) {
									// Check if the updated tab is the last focused window's active tab.
									const active_tab = windowInfo.tabs.filter(tab => tab.active)[0];
									if (tabId === active_tab.id) {
										// If the active tab has a URL:
										if (changeInfo.url) {
											// Check if the domain (i.e. the website) changed.
											let url = new URL(changeInfo.url);
											if (url.hostname !== event.website) {
												// Change events.
												Change(date, url);

												// Log.
												if (log) {
													console.log(tabId.toString() + "\n" + "Website change in active tab."+ "\n" + event.website);
												}
											}
										}
										// If the active tab doesn't have a URL:
										else {
											// Change to eventless.
											Change(date);
										}
									}
								}
							});
					}
				});
		}
	});

	/*
		When a tab in any window is updated:
		1. Check if the update is a favicon URL change.
		2. Check if the website's URL scheme matches.
		3. Use node-vibrant to find prominent colors in the favicon without downsampling.
		4. Create a minimized palette to find the most prominent swatch.
		5. Count the vibrant (LightVibrant + Vibrant + DarkVibrant) and muted (LightMuted + Muted + DarkMuted) populations.
		6. Populate the minimized palette and count the populations.
			a. Check if the swatch is NOT undefined.
			b. Create a minimized swatch.
			c. Accumulate populations.
		7. If the minimized palette isn't empty:
			a. Normalize prominences by the proportion of the opposite population if it's not 0.
			b. H: Multiply prominences by 1 + (average minimum angle in τ radians to other hues).
			c. S: Multiply prominences by 1 + S.
			d. L: Multiply prominences by 1 + (0.5 - |0.5 - L|)/0.5.
			e. Sort the swatches in decreasing prevalence.
			f. Sort the swatches in decreasing prominence.
			g. Choose the swatch with the highest saturation and luminance closest to 0.5. Choose the most prevalent if they are equal.
			h. Dye events on a website a specific color.
	*/

	browser.tabs.onUpdated.addListener((tabId, changeInfo, tabInfo) => {
		// Check if the update is a favicon URL change.
		if (changeInfo.favIconUrl) {
			// Check if the website's URL scheme matches.
			let url = new URL(tabInfo.url);
			if (schemes.includes(url.protocol)) {
				// Use node-vibrant to find prominent colors in the favicon without downsampling.
				Vibrant.from(changeInfo.favIconUrl)
					.quality(1)
					.getPalette()
					.then(palette => {
						// Create a minimized palette to find the most prominent swatch.
						let p = {};

						// Count the vibrant (LightVibrant + Vibrant + DarkVibrant) and muted (LightMuted + Muted + DarkMuted) populations.
						let V = 0, M = 0;

						// Populate the minimized palette and count the populations.
						for (let swatch in palette) {
							// Check if the swatch is NOT undefined.
							if (palette[swatch]) {
								// Create a minimized swatch.
								p[swatch] = {
									hex: palette[swatch].hex,
									hsl: palette[swatch].hsl,
									prominence: palette[swatch].population
								}

								// Accumulate populations.
								// Vibrant.
								if (swatch.search(/Vibrant/i) !== -1) {
									V += palette[swatch].population;
								}
								// Muted.
								else {
									M += palette[swatch].population;
								}
							}
						}

						// If the minimized palette isn't empty:
						if (Object.keys(p).length > 0) {
							// Normalize prominences by the proportion of the opposite population if it's not 0.
							for (let s in p) {
								// Vibrant.
								if (s.search(/Vibrant/i) !== -1) {
									p[s].prominence *= M/(V + M) || 1;
								}
								// Muted.
								else {
									p[s].prominence *= V/(V + M) || 1;
								}
							}

							// H: Multiply prominences by 1 + (average minimum angle in π radians to other hues).
							for (let s in p) {
								// Find the average minimum angle in τ radians to other hues.
								let avg = 0;
								for (let s2 in p) {
									if (s2 !== s) {
										let theta = Math.abs(p[s].hsl[0] - p[s2].hsl[0]);
										avg += Math.min(theta, 1 - theta);
									}
								}
								avg /= (Object.keys(p).length - 1) || Infinity;
								p[s].prominence *= 1 + 2*avg;
							}

							// S: Multiply prominences by 1 + S.
							for (let s in p) {
								p[s].prominence *= 1 + p[s].hsl[1];
							}

							// L: Multiply prominences by 1 + (0.5 - |0.5 - L|)/0.5.
							for (let s in p) {
								p[s].prominence *= 2 - Math.abs(1 - 2*p[s].hsl[2]);
							}

							// Sort the swatches in decreasing prevalence.
							let prevalence = Object.values(palette).filter(swatch => swatch).sort((a, b) => {
								return b.population - a.population;
							});

							// Sort the swatches in decreasing prominence.
							let prominence = Object.values(p).sort((a, b) => {
								return b.prominence - a.prominence;
							});

							// Choose the swatch with the highest saturation and luminance closest to 0.5. Choose the most prevalent if they are equal.
							let color = ((1 + prevalence[0].hsl[1])*(2 - Math.abs(1 - 2*prevalence[0].hsl[2])) >= ((1 + prominence[0].hsl[1])*(2 - Math.abs(1 - 2*prominence[0].hsl[2])))) ? prevalence[0].hex : prominence[0].hex;

							// Log.
							if (log) {
								console.log(url.hostname + "\n" + "\n" + "Prevalence:" + "\n", palette, "\n" + prevalence[0].hex + "\n", "\n" + "Prominence:" + "\n", p, "\n" + prominence[0].hex + "\n", "\n" + "Color:" + "\n" + color);
							}

							// Dye events on a website a specific color.
							Dye(url, color)
								// Log.
								.then(message => {
									if (log) {
										if (message) {
											console.log(message);
										}
									}
								});
						}
					});
			}
		}
	});

	/*
		Forget, then create a reminder to forget once per day.
	*/

	Forget()
		// Log.
		.then(message => {
			if (log) {
				if (message) {
					console.log(message);
				}
			}
		});
	browser.alarms.create("forget", {periodInMinutes: 24*60});
});