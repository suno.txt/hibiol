
/*
	The Hibiol database.
*/

function Open() {
	return new Promise((resolve, reject) => {
		// Open the database.
		let request = window.indexedDB.open("Hibiol", 1);

		// Return an error if the database cannot be opened.
		request.addEventListener("error", event => {
			reject(event.target.error);
		});
		request.addEventListener("blocked", event => {
			reject(new Error("An earlier version of the database is still open."));
		});

		// Upgrade the database.
		request.addEventListener("upgradeneeded", event => {
			const database = event.target.result;

			/*
				Name [version]
	
				Store
					key				※
					index
						single		†
						multi		‡
						unique		†* or ‡*
					property
			*/

			/*
				Hibiol 1
	
				Events
					begin			※
					end				†*
					website			†

				Colors
					website			※
					color

				Hidden
					website			※
			*/

			if (event.oldVersion < 1) {
				// Events.
				let Events = database.createObjectStore("Events", {keyPath: "begin"});
				Events.createIndex("end", "end", {unique: true});
				Events.createIndex("website", "website", {unique: false});
	
				// Colors
				//let Colors = database.createObjectStore("Colors", {keyPath: "website"});
				database.createObjectStore("Colors", {keyPath: "website"});

				// Hidden
				//let Hidden = database.createObjectStore("Hidden", {keyPath: "website"});
				database.createObjectStore("Hidden", {keyPath: "website"});
			}
		});

		// Return the database.
		request.addEventListener("success", event => {
			resolve(event.target.result);
		});
	});
}

export default Open;