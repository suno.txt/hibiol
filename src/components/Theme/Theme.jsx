// React.
import React, {Component} from "react";
// Packages.
import browser from "webextension-polyfill";
import SunCalc from "suncalc";
// etc.
import "./nighttime.scss";
import "./astronomical_twilight.scss";
import "./nautical_twilight.scss";
import "./civil_twilight.scss";
import "./sunrise_sunset.scss";
import "./golden_hour.scss";
import "./daytime.scss";

class Theme extends Component {
	constructor(props) {
		super(props);

		this.state = {
			date: new Date(),
			latitude: undefined,
			longitude: undefined
		};

		// Time-keeper.
		setTimeout(() => {
			setInterval(() => {
				this.setState({
					date: new Date()
				});
			}, 1000);
			this.setState({
				date: new Date()
			});
		}, 999 - new Date().getMilliseconds());

		// Position-keeper.
		browser.storage.onChanged.addListener((changes, area) => {
			let s = new Set([
				"latitude",
				"longitude"
			]);
			let c = {};
			Object.keys(changes)
				.forEach(key => {
					if (s.has(key)) {
						c[key] = changes[key].newValue;
					}
				});
			if (Object.keys(c).length) {
				this.setState(c);
			}
		});

		// Restore position.
		browser.storage.local.get(["latitude", "longitude"])
			.then(result => {
				this.setState({
					latitude: result.latitude || this.state.latitude,
					longitude: result.longitude || this.state.longitude
				});
			});
	}

	render() {
		if (this.state.latitude !== undefined && this.state.longitude !== undefined) {
			const body = document.body;
			const solar_altitude_angle = (180/Math.PI)*SunCalc.getPosition(this.state.date, this.state.latitude, this.state.longitude).altitude;
			// Night.
			if (solar_altitude_angle < -18) {
				body.className = "nighttime";
			}
			// Astronomical twilight.
			else if (solar_altitude_angle < -12) {
				body.className = "astronomical_twilight";
			}
			// Nautical twilight.
			else if (solar_altitude_angle < -6) {
				body.className = "nautical_twilight";
			}
			// Civil twilight.
			else if (solar_altitude_angle < -0.833) {
				body.className = "civil_twilight";
			}
			// Sunrise and sunset.
			else if (solar_altitude_angle < -0.3) {
				body.className = "sunrise_sunset";
			}
			// Golden hour.
			else if (solar_altitude_angle < 6) {
				body.className = "golden_hour";
			}
			// Day.
			else {
				body.className = "daytime";
			}
		}

		return (
			<React.Fragment></React.Fragment>
		);
	}
}

export default Theme;