// React.
import React, {Component} from "react";
import {Route} from "react-router-dom";
// etc.
import Home from "../Home/Home";
import Settings from "../Settings/Settings";
import Open from "../../scripts/_database";

class Content extends Component {
	constructor(props) {
		super(props);

		this.state = {
			database: undefined
		};

		Open().then(database => {
			this.setState({
				database: database
			});
		});
	}

	render() {
		return (
			<main className="content">
				<Route
					exact
					path="/"
					render={() => (
						<Home database={this.state.database}/>
					)}
				/>

				<Route
					exact
					path="/settings"
					render={() => (
						<Settings database={this.state.database}/>
					)}
				/>
			</main>
		);
	}

	componentWillUnmount() {
		if (this.state.database) {
			this.state.database.close();
		}
	}
}

export default Content;