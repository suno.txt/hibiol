// React.
import React, {Component} from "react";
import PropTypes from "prop-types";
// etc.
import Chronicle from "../Chronicle/Chronicle";
import Clock from "../Clock/Clock";

class Home extends Component {
	render() {
		return (
			<React.Fragment>
				<Chronicle database={this.props.database}/>

				<Clock/>
			</React.Fragment>
		);
	}
}

Home.propTypes = {
	database: PropTypes.instanceOf(IDBDatabase)
}

export default Home;