// React.
import React, {Component} from "react";
import PropTypes from "prop-types";
// Packages.
import browser from "webextension-polyfill";
// etc.
import "./Chronicle.scss";
import Loom from "../Loom/Loom";

class Chronicle extends Component {
	constructor(props) {
		super(props);

		let date = new Date();
		this.state = {
			date: date,
			timezone_offset: date.getTimezoneOffset(),
			date_offset: undefined,
			present: Array(5).fill({}),
			past: Array(5).fill({}),
			colors: {}
		};

		// Time-keeper.
		setTimeout(() => {
			setInterval(() => {
				let c = {};
				let date = new Date();
				// A minute elapsed.
				if (this.state.date.getMinutes() !== date.getMinutes()) {
					c.date = date;
				}
				// Timezone change.
				if (this.state.timezone_offset !== date.getTimezoneOffset()) {
					c.timezone_offset = date.getTimezoneOffset();
				}
				// Make changes if necessary.
				if (Object.keys(c).length) {
					this.setState(c);
				}
			}, 1000);
			let c = {};
			let date = new Date();
			// A minute elapsed.
			if (this.state.date.getMinutes() !== date.getMinutes()) {
				c.date = date;
			}
			// Timezone change.
			if (this.state.timezone_offset !== date.getTimezoneOffset()) {
				c.timezone_offset = date.getTimezoneOffset();
			}
			// Make changes if necessary.
			if (Object.keys(c).length) {
				this.setState(c);
			}
		}, 999 - new Date().getMilliseconds());

		// Bindings.
		this.Date_Offset_Change = this.Date_Offset_Change.bind(this);

		// Restore preferences.
		browser.storage.local.get(["date_offset"])
			.then(result => {
				this.setState({
					date_offset: result.date_offset || 1,
				});
			});
	}

	/*
		Change the date offset.
	*/

	Date_Offset_Change(event) {
		let date_offset = parseInt(event.target.value, 10);
		this.setState({
			date_offset: date_offset
		});
		browser.storage.local.set({
			date_offset: date_offset
		});
	}

	/*
		Recall events on a date (local).

		Returns an array of objects:

		[
			{
				website: String,
				events: [
					{
						begin: Date,
						end: Date
					}
				],
				duration: Number
			},
			...
		]
	*/

	async Recall(date) {
		if (date) {
			let database = this.props.database;
			// Find the beginning and ending of the date.
			let lower = new Date(date.getFullYear(), date.getMonth(), date.getDate());
			let upper = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59, 999);
			let chronicle = await new Promise((resolve, reject) => {
				// Open a transaction.
				let transaction = database.transaction("Events", "readonly");
				transaction.addEventListener("abort", event => {
					reject(event.target.error);
				});

				// Find the last end.
				let Events = transaction.objectStore("Events");
				let request = Events.openCursor(IDBKeyRange.upperBound(upper), "prev");
				request.addEventListener("success", event => {
					// No cursor is returned if no matching records exist.
					if (event.target.result) {
						// Check if the last end is later than the lower bound.
						let last_end = event.target.result.value.end;
						if (lower <= last_end) {
							// Find events within the window.
							let end = Events.index("end");
							let request = end.getAll(IDBKeyRange.bound(lower, last_end));
							request.addEventListener("success", event => {
								resolve(event.target.result);
							});
						}
						else {
							resolve([]);
						}
					}
					else {
						resolve([]);
					}
				});
			});

			if (chronicle.length) {
				// Sort events.
				chronicle.sort((a, b) => a.begin - b.begin);

				// Trim the earliest and latest events if they extend beyond the window.
				chronicle[0].begin = new Date(Math.max(chronicle[0].begin, lower));
				chronicle[chronicle.length - 1].end = new Date(Math.min(chronicle[chronicle.length - 1].end, upper));
			}

			// Separate events by website.
			chronicle = chronicle.reduce((chronicle, event) => {
				(chronicle[event.website] = chronicle[event.website] || []).push({begin: event.begin, end: event.end});
				return chronicle;
			}, {});

			// Meld events apart by 1 second or less.
			Object.keys(chronicle).forEach(website => {
				chronicle[website] = chronicle[website].reduce((events, event) => {
						(events.length) ? ((event.begin.getTime() <= events[events.length - 1].end.getTime() + 1000) ? events[events.length - 1].end = event.end : events.push(event)) : events.push(event);
						return events;
					}, []);
			});

			// Change to an array and calculate durations.
			chronicle = Object.keys(chronicle).reduce((a, website) => {
				a.push({
					website: website,
					events: chronicle[website],
					duration: chronicle[website].reduce((duration, event) => duration + (event.end.getTime() - event.begin.getTime()), 0)
				});
				return a;
			}, []);

			return chronicle;
		}
		else {
			return [];
		}
	}

	/*
		Retrieve colors.

		Returns an object:

		{
			[website]: {
				color: String
			},
			...
		}
	*/

	async Dye(websites = []) {
		websites = websites.filter(e => e);
		if (websites.length) {
			return await new Promise((resolve, reject) => {
				let database = this.props.database;
				// Open a transaction.
				let transaction = database.transaction("Colors", "readonly");
				transaction.addEventListener("abort", event => {
					reject(event.target.error);
				});

				// Find the properties of the specified websites.
				let result = {};
				let Colors = transaction.objectStore("Colors");
				// Requests are sequentially resolved.
				websites.forEach((website, i) => {
					let request = Colors.get(website);
					request.addEventListener("success", event => {
						// No result is returned if no matching records exist.
						if (event.target.result) {
							result[website] = event.target.result.color;
						}
						if (i === websites.length - 1) {
							resolve(result);
						}
					});
				});
			});
		}
		else {
			return {};
		}
	}

	/*
		Retrieve websites to hide.

		Returns an array of strings (websites).
	*/

	Hide() {
		return new Promise((resolve, reject) => {
			let database = this.props.database;
			// Open a transaction.
			let transaction = database.transaction("Hidden", "readonly");
			transaction.addEventListener("abort", event => {
				reject(event.target.error);
			});

			// Find the websites to hide.
			let Hidden = transaction.objectStore("Hidden");
			let request = Hidden.getAllKeys();
			request.addEventListener("success", event => {
				resolve(event.target.result);
			});
		});
	}

	render() {
		return (
			<div className="chronicle">
				<section className="present">
					<header className="present_header">
						<div className="date_day" key={new Date(this.state.date.getFullYear(), this.state.date.getMonth(), this.state.date.getDate()).getDate()}>
							<h2 className="date">
								{
									new Date(this.state.date.getFullYear(), this.state.date.getMonth(), this.state.date.getDate()).getDate()
								}
							</h2>

							<h3 className="day">
								{
									new Date(this.state.date.getFullYear(), this.state.date.getMonth(), this.state.date.getDate()).toLocaleString(navigator.language, {weekday: "short"})
								}
							</h3>
						</div>
					</header>

					{
						this.state.present.map((e, i) => (
							<Loom
								key={i}
								website={e.website}
								events={e.events}
								duration={e.duration}
								color={this.state.colors[e.website]}
							/>
						))
					}
				</section>

				<section className="past">
					<header className="past_header">
						<div className="date_day" key={new Date(this.state.date.getFullYear(), this.state.date.getMonth(), this.state.date.getDate() - this.state.date_offset).getDate()}>
							<h2 className="date">
								{
									this.state.date_offset && new Date(this.state.date.getFullYear(), this.state.date.getMonth(), this.state.date.getDate() - this.state.date_offset).getDate()
								}
							</h2>

							<h3 className="day">
								{
									this.state.date_offset && new Date(this.state.date.getFullYear(), this.state.date.getMonth(), this.state.date.getDate() - this.state.date_offset).toLocaleString(navigator.language, {weekday: "short"})
								}
							</h3>
						</div>

						<select value={this.state.date_offset} onChange={this.Date_Offset_Change}>
							{
								Array(7).fill().map((e, i) => (
									<option key={i + 1} value={i + 1}>{i + 1} ← ●</option>
								))
							}
						</select>
					</header>

					{
						this.state.past.map((e, i) => (
							<Loom
								key={i}
								website={e.website}
								events={e.events}
								duration={e.duration}
								color={this.state.colors[e.website]}
							/>
						))
					}
				</section>
			</div>
		);
	}

	componentDidUpdate(prevProps, prevState) {
		// The database connected, a minute elapsed, the timezone changed, or the date offset changed.
		if (prevProps.database !== this.props.database || prevState.date.getMinutes() !== this.state.date.getMinutes() || prevState.timezone_offset !== this.state.timezone_offset || prevState.date_offset !== this.state.date_offset) {
			// Check if the database is open.
			if (this.props.database) {
				(async () => {
					// Weave undyed Hibiol if the date or timezone changed.
					let c = {};

					// Date or timezone change.
					if (prevState.date.getDate() !== this.state.date.getDate() || prevState.timezone_offset !== this.state.timezone_offset) {
						c.present = Array(5).fill({});
						c.past = Array(5).fill({});
					}

					// Date offset change.
					if (prevState.date_offset !== this.state.date_offset) {
						c.past = Array(5).fill({});
					}

					// Weave undyed Hibiol.
					if (Object.keys(c).length) {
						this.setState(c);
					}

					// Recall events and filter hidden websites.
					let present = await this.Recall(this.state.date);
					let past = await this.Recall(new Date(this.state.date.getFullYear(), this.state.date.getMonth(), this.state.date.getDate() - this.state.date_offset));

					// Retrieve websites to hide.
					let hidden = await this.Hide();
					// Filter hidden websites if needed.
					if (hidden.length) {
						hidden = new RegExp(hidden.reduce((a, e) => `${a}${(a.length) ? "|" : ""}(${e})`, String.raw``));
						present = present.filter(e => hidden.test(e));
						past = past.filter(e => hidden.test(e));
					}

					// Find the 3 websites with the highest duration and 2 websites from the remaining with the highest frequency.

					// Sort by descending duration.
					present.sort((a, b) => b.duration - a.duration);
					past.sort((a, b) => b.duration - a.duration);

					// Keep the first 3 websites, then sort the rest by descending frequency and keep the first 2 of the rest.
					present = [...present.slice(0, 3), ...present.slice(3, present.length).sort((a, b) => b.events.length - a.events.length).slice(0, 2)];
					past = [...past.slice(0, 3), ...past.slice(3, past.length).sort((a, b) => b.events.length - a.events.length).slice(0, 2)];

					// Extend to length 5.
					present = [...present, ...Array(Math.max(0, 5 - present.length)).fill({})];
					past = [...past, ...Array(Math.max(0, 5 - past.length)).fill({})];

					// Retrieve colors.
					let colors = await this.Dye(Array.from(new Set([...present.map(e => e.website), ...past.map(e => e.website)])));

					// Weave dyed Hibiol.
					this.setState({
						present: present,
						past: past,
						colors: colors
					});
				})();
			}
		}
	}
}

Chronicle.propTypes = {
	database: PropTypes.instanceOf(IDBDatabase),
}

export default Chronicle;