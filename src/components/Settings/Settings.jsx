// React.
import React, {Component} from "react";
import PropTypes from "prop-types";

class Settings extends Component {
	render() {
		return (
			<React.Fragment>
				<p>Settings</p>
			</React.Fragment>
		);
	}
}

Settings.propTypes = {
	database: PropTypes.instanceOf(IDBDatabase)
}

export default Settings;