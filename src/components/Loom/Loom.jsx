// React.
import React, {Component} from "react";
import PropTypes from "prop-types";
// etc.
import "./Loom.scss";

class Loom extends Component {
	render() {
		return (
			<div className="loom">
				<header className="loom_header" key={this.props.website}>
					<p className="place">
						{
							this.props.website &&
							this.props.website.replace(/^w{3}\d*\./, "")
						}
					</p>
					<p className="time">
						{
							this.props.duration &&
							(() => {
								let duration = this.props.duration;
								let hours = Math.floor(duration/(60*60*1000));
								duration -= hours*(60*60*1000);
								let minutes = Math.floor(duration/(60*1000));
								return (
									<React.Fragment>
										{
											hours.toString()
										}
										<span className="colon">:</span>
										{
											minutes.toString().padStart(2, "0")
										}
									</React.Fragment>
								);
							})()
						}
					</p>
				</header>

				<div className="Hibiol">
					<div className="wrapper">
						{
							this.props.events &&
							this.props.events.map(event => (
								<div
									className="event"
									key={event.begin.getTime()}
									style={
										{
											left: `${(event.begin.getHours()*60*60*1000 + event.begin.getMinutes()*60*1000 + event.begin.getSeconds()*1000 + event.begin.getMilliseconds())/(24*60*60*1000)*100}%`,
											width: `${(event.end.getTime() - event.begin.getTime())/(24*60*60*1000)*100}%`,
											backgroundImage: `linear-gradient(${this.props.color}, ${this.props.color})`
										}
									}
								></div>
							))
						}
					</div>
				</div>
			</div>
		);
	}
}

Loom.propTypes = {
	// Website.
	website: PropTypes.string,
	// Events.
	events: PropTypes.arrayOf(PropTypes.shape({begin: PropTypes.instanceOf(Date).isRequired, end: PropTypes.instanceOf(Date).isRequired}).isRequired),
	// Duration.
	duration: PropTypes.number,
	// Color.
	color: PropTypes.string
};

Loom.defaultProps = {
	// Color.
	color: "hsl(0, 0%, 50%)"
};

export default Loom;