// React.
import React, {Component} from "react";
// etc.
import "./Content_Header.scss";

class Content_Header extends Component {
	constructor(props) {
		super(props);

		this.state = {
			date: new Date()
		};

		// Time-keeper.
		setTimeout(() => {
			setInterval(() => {
				this.setState({
					date: new Date()
				});
			}, 1000);
			this.setState({
				date: new Date()
			});
		}, 999 - new Date().getMilliseconds());
	}

	render() {
		return (
			<header className="content_header">
				<h1 className="month" key={this.state.date.getMonth()}>
					{
						this.state.date.toLocaleString(navigator.language, {month: "numeric"})
							.replace(/\D/g, "")
					}
				</h1>

				<h2 className="time">
					{
						this.state.date.toLocaleString(navigator.language, {hour: "numeric"})
							.replace(/\D/g, "")
					}
					<span className="colon">:</span>
					{
						this.state.date.toLocaleString(navigator.language, {minute: "2-digit"})
							.replace(/\D/g, "")
							.padStart(2, "0")
					}
				</h2>
			</header>
		);
	}
}

export default Content_Header;