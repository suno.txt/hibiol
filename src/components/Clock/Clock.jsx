// React.
import React, {Component} from "react";
// etc.
import "./Clock.scss";

const filter = (() => {

	/*
		Approximate the upper half of a band-pass filter with normal skirts, width 1, and offset 0.5 using √(2Π(σ^2))⋅N(0, 1) (unscaled standard normal distribution).
	*/

	// Skirt width as a proportion of filter half-width.
	const skirt_width = 0.6;
	const max_opacity = 0.6;

	let filter = [];
	if (skirt_width < 1) {
		filter.push(`hsla(0, 0%, 100%, ${max_opacity}) 0%`);
	}
	// Skirt.
	for (let x = 0; x < 3; x += 0.1) {
		filter.push(`hsla(0, 0%, 100%, ${Math.min(max_opacity, 1)*Math.E**(-(x**2)/2)}) ${(1 - (1 - x/3)*Math.min(skirt_width, 1))*100}%`);
	}
	filter.push("transparent 100%");
	return filter.reduce((color_stop_list, color_stop, i) => (color_stop_list + color_stop + ((i < (filter.length - 1)) ? ", " : "")), "");
})();

class Clock extends Component {
	constructor(props) {
		super(props);

		this.state = {
			date: new Date()
		};

		// Time-keeper.
		setTimeout(() => {
			setInterval(() => {
				this.setState({
					date: new Date()
				});
			}, 1000);
			this.setState({
				date: new Date()
			});
		}, 999 - new Date().getMilliseconds());
	}

	render() {
		// Create arrays for element mapping.
		let hours = [];
		for (let i = 0; i <= 24; ++i) {
			// 1/4 day.
			if (i % 6 === 0) {
				hours.push(<p>{new Date(this.state.date.getFullYear(), this.state.date.getMonth(), this.state.date.getDate(), i).toLocaleString(navigator.language, {"hour": "numeric"}).replace(/\D/g, "")}</p>);
			}
			// 1/8 day.
			else if (i % 3 === 0) {
				hours.push(<p className="major"></p>);
			}
			// 1/24 day.
			else {
				hours.push(<p className="minor"></p>);
			}
		}

		return (
			<div className="clock">
				<div className="wrapper">
					<div
						className="dot"
						key={`${this.state.date.getDate()} ${this.state.date.getTimezoneOffset()}`}
						style={
							{
								left: `${(this.state.date.getHours()*60*60*1000 + this.state.date.getMinutes()*60*1000 + this.state.date.getSeconds()*1000 + this.state.date.getMilliseconds())/(24*60*60*1000)*100}%`,
								mask: `radial-gradient(closest-side, ${filter}) alpha`,
								WebkitMaskImage: `radial-gradient(closest-side, ${filter})`
							}
						}
					></div>

					<div className="hours">
						{
							hours.map((hour, i) =>
								<div key={i} className="hour">{hour}</div>
							)
						}
					</div>
				</div>
			</div>
		);
	}
}

export default Clock;