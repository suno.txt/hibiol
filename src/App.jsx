// React.
import React, {Component} from "react";
// etc.
import "./App.scss";
import Theme from "./components/Theme/Theme";
import Content_Header from "./components/Content_Header/Content_Header";
import Content from "./components/Content/Content";

class App extends Component {
	render() {
		return (
			<React.Fragment>
				<Theme/>

				<Content_Header/>

				<Content/>
			</React.Fragment>
		);
	}

	componentDidMount() {
		window.addEventListener("languagechange", () => {
			this.forceUpdate();
		});
	}
}

export default App;