// React.
import React from "react";
import ReactDOM from "react-dom";
import {MemoryRouter} from "react-router-dom";
// Packages.
import "normalize.css";
import "typeface-frank-ruhl-libre";
import "typeface-spectral";
// etc.
import App from "./App";

ReactDOM.render(<MemoryRouter><App/></MemoryRouter>, document.getElementById("root"));