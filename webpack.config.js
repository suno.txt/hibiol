const path = require('path');

/*
	Config for bundling background and content scripts.

	Script bundle entry points are expected to be in "./src/scripts/" for tidiness.

	Each bundle is written to "./build/scripts/[key].js", where [key] is the bundle's entry point key.
	Entry point keys can be paths relative to "./build/scripts", letting us write the bundle to other places.
	i.e. The entry point key "[path]/[name]" will write the bundle to "./build/scripts/[path]/[name].js".
*/

module.exports = {
	mode: "production",
	context: path.resolve(__dirname, "src/scripts"),
	entry: {
		// Background scripts.
		"background/chronicle.js": "./background/chronicle.js",
		"background/position.js": "./background/position.js"
		// Content scripts.
	},
	output: {
		path: path.resolve(__dirname, "build/scripts"),
		filename: "[name]"
	},
	/*
		node-vibrant 3.2.0 alpha uses worker-loader.

		The worker-loader script is written to "./build/scripts/[name]". Scripts relying on the worker-loader script,
		however, look for it at "./build/[publicPath][name]". To accomodate for this, we set publicPath to "scripts/".
	*/
	module: {
		rules: [
			{
				test: /\.worker\.js$/,
				use: {
					loader: 'worker-loader',
					options: {
						name: "web_worker/web_worker.js",
						publicPath: "scripts/"
					}
				}
			}
		]
	}
};