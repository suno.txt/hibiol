# Hibiol — Privacy Policy

_Last update: 2020 January 25_

Hibiol is an open source extension which visualizes your browsing history on the new tab page. A limited chronicle of your browsing history is kept on your device to create these visualizations.

Hibiol also cycles through themes for different times of day. To determine which theme should be used and to support offline functionality, your device's last known location is kept locally as well.

## Table of Contents
* [What data does Hibiol keep?](#what-data-does-hibiol-keep)
* [How does Hibiol use your data?](#how-does-hibiol-use-your-data)
* [Where does Hibiol keep your data?](#where-does-hibiol-keep-your-data)

## What data does Hibiol keep?
Hibiol keeps your browsing history and your device's last known location.

The browsing history Hibiol keeps is a limited chronicle of when and what website is open in the active tab when your browser is in focus. This differs from the browsing history your browser keeps which is a chronicle of when specific webpages are opened regardless of tab or browser status.

Browsing history older than 8 days is forgotten on extension startup and every 24 hours afterwards.

Hibiol is disabled in private browsing mode.

## How does Hibiol use your data?
Hibiol creates visualizations on the new tab page using the limited chronicle of your browsing history.

Hibiol uses your device's last known location to calculate the location's solar altitude. The solar altitude is used to cycle through themes for daytime, golden hour, sunrise and sunset, civil twilight, nautical twilight, astronomical twilight, and nighttime.

## Where does Hibiol keep your data?
All data is kept on your device and is only accessible to you and Hibiol.